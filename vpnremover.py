#!/usr/bin/env python3

"""vpnremover.py: FortiOS interface-mode IPSec VPN config remover"""

__author__ = "Lukasz Korbasiewicz"
__maintainer__ = "Lukasz Korbasiewicz"
__email__ = "lkorbasiewicz@fortinet.com"
__version__ = "1.0"
__status__ = "Production"

import os

def hasvdoms():
    ans = 0
    for _ in cfg:
        if _ == 'config vdom\n':
            ans = 1
            break
    return ans


BLUE ='\033[94m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RED = '\033[91m'
NORM = '\033[0m'

# Clear the terminal
print(chr(27) + "[2J")

print(GREEN + '\n\n\n\n\nWelcome!\nThis is FortiOS VPN Remover v1.0\n' + NORM)


# Open path
path = "./"
dirs = os.listdir(path)

# Create empty filelist
cfgfiles = []
cfgindex = 0

# This will add all .conf to list "cfgfiles"
for file in dirs:
    if ".conf" in file:
        cfgfiles.append(file)
        print(str(cfgindex) + " - " + file)
        cfgindex += 1
if cfgindex > 1:
    cfgindex = input("\nWhich file you want to open? Type number only! > ")
    cfgindex = int(cfgindex)
    cfgfile = cfgfiles[cfgindex]
else:
    print('\nOnly one config file found in current directory, I will use it')
    cfgfile = cfgfiles[0]
print('\nReading file: "' + cfgfile + '"\n')
f = open(cfgfile)
cfg = f.readlines()
print('\nDone!\n')
f.close()

vpntunnel = input("\nWhich VPN tunnel you would like to remove? Type Phase 1 name > ")

print('\nSyntax to remove "' + vpntunnel + '" VPN tunnel from "' + cfgfile + '":\n')

confstartindex = 0
vdomstart = []
vdomend = []
vdlist = []
hasvd = hasvdoms()

if hasvd:
    for num, line in enumerate(cfg):
        if line == 'config global\n':
            confstartindex = num
    cfg = cfg[confstartindex:]

    for num, line in enumerate(cfg):
        if line == 'config vdom\n':
            vdomstart.append(num)

    for ind, startline in enumerate(vdomstart):
        for num, line in enumerate(cfg):
            if line == 'config vdom\n' and num > startline:
                vdomend.append(num)
                break
    vdomend.append(len(cfg))

    vdnum = range(len(vdomstart))
    rtstart = []
    rtend = []
    polstart = []
    polend = []
    p1start = []
    p1end = []
    p2start = []
    p2end = []
    delroutes = []
    for vd in vdnum:
        vdstart = vdomstart[vd]
        vdend = vdomend[vd]
        vdname = cfg[vdstart+1].strip(' edit').strip('\n')
        cfgslice = cfg[vdstart:vdend]
        vdlist.append(vdname)

    # ROUTES
        checker = 0
        for num, line in enumerate(cfgslice):
            if line == 'config router static\n':
                rtstart.append(num)
                checker = num
                break
        if checker == 0:
            rtstart.append(0)
            rtend.append(0)
        else:
            for num, line in enumerate(cfgslice):
                if line == 'end\n' and num > rtstart[vd]:
                    rtend.append(num)
                    break
        rtslice = cfgslice[rtstart[vd]:rtend[vd]]
        rtid = None
        delroutes = []
        for line in rtslice:
            if line.startswith('    edit '):
                rtid = int(line.strip(' edit ').strip('\n'))
            if line == '        set device "' + vpntunnel + '"\n':
                delroutes.append(rtid)
        if delroutes:
            print('config vdom\nedit ' + vdname + '\nconfig router static')
            for rt in delroutes:
                print('delete ' + str(rt))
            print('end\nend')

    # POLICIES
        checker = 0
        for num, line in enumerate(cfgslice):
            if line == 'config firewall policy\n':
                polstart.append(num)
                checker = num
                break
        if checker == 0:
            polstart.append(0)
            polend.append(0)
        else:
            for num, line in enumerate(cfgslice):
                if line == 'end\n' and num > polstart[vd]:
                    polend.append(num)
                    break
        polslice = cfgslice[polstart[vd]:polend[vd]]
        polid = None
        delpolicies = []
        for line in polslice:
            if line.startswith('    edit '):
                polid = int(line.strip(' edit ').strip('\n'))
            if line == '        set srcintf "' + vpntunnel + '"\n' or line == '        set dstintf "' + vpntunnel + '"\n':
                delpolicies.append(polid)
        if delpolicies:
            print('config vdom\nedit ' + vdname + '\nconfig firewall policy')
            for pol in delpolicies:
                print('delete ' + str(pol))
            print('end\nend')
    # TUNNELS
        checker = 0
        for num, line in enumerate(cfgslice):
            if line == 'config vpn ipsec phase2-interface\n':
                p2start.append(num)
                checker = num
                break
        if checker == 0:
            p2start.append(0)
            p2end.append(0)
        else:
            for num, line in enumerate(cfgslice):
                if line == 'end\n' and num > p2start[vd]:
                    p2end.append(num)
                    break
        p2slice = cfgslice[p2start[vd]:p2end[vd]]
        p2id = None
        delp2 = []
        for line in p2slice:
            if line.startswith('    edit '):
                p2id = line.strip(' edit ').strip('\n')
            if line == '        set phase1name "' + vpntunnel + '"\n':
                delp2.append(p2id)
        if delroutes:
            print('config vdom\nedit ' + vdname + '\nconfig vpn ipsec phase2-interface')
            for p2 in delp2:
                print('delete ' + p2)
            print('end')
            print('config vpn ipsec phase1-interface\ndelete ' + vpntunnel + '\nend\nend')

