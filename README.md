# README #

FortiOS interface-mode IPSec VPN config remover

## Files ##

### vpnremover.py ###

This script will generate syntax that deletes an interface-mode IPSec VPN tunnels with all related objects

Run it from within the directory containing firewall config(s).

It takes no command line arguments

### README.md ###

This file